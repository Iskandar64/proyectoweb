module.exports = [
	"Action",
	"Action-Adventure",
	"Adventure",
	"Beat 'Em Up",
	"Casual",
	"Christian",
	"Fighting",
	"FPS",
	"Hack 'N Slash",
	"Metroidvania",
	"MMO",
	"Multi-Player",
	"MOBA",
	"Open World",
	"Platform",
	"Racing",
	"Role-Playing",
	"RTS",
	"Simulation",
	"Single-Player",
	"Stealth",
	"Strategy",
	"Sports",
	"Survival Horror",
	"TBS",
	"Tower Defense",
	"Visual Novel"
]
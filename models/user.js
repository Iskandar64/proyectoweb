const mongoose = require('mongoose');
const Joi = require('joi');
const status = require('./gameStatus');
const plat = require('./platforms');
const jwt = require('jsonwebtoken');


const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    birth: {
        type: Date,
        required: true,
        get: value => value.toDateString()
    },
    password: {
        type: String,
        required: true,
    },
    platforms: {
        type: [{
            type: String,
            enum: plat
        }],
        min: 1,
        required: true,
        get : v => v.map(value => value.charAt(0).toUpperCase()+value.slice(1))
    },
    description: {
    	type: String,
        maxlength: 300,
        minlength: 3
    },
    admin: {
        type: Boolean,
        default: false
    },
    seguidores: {
        type: [{
            type: String
        }]
    }
}, { toJSON: { getters: true }}); // Si quieren que sus getters funcionen en JSON hay que agregar el toJSON, si quieren que funcionen en objetos hay que agregar toObject


userSchema.methods.generarAuthToken = function() {
    const token = jwt.sign({ _id: this._id }, process.env.SECRET);
    return token;
}

const User = mongoose.model('User', userSchema);
function validateUser(user){
    const schema = {
        name: Joi.string().min(3).max(50).required().trim(),
        email: Joi.string().email().required(),
        birth: Joi.date().required(),          
        password: Joi.string().required().trim(),
        platforms: Joi.array().required().min(1),
        description: Joi.string().required().trim(),
        admin: Joi.boolean(),
        seguidores: Joi.array().items()
    }
    return Joi.validate(user, schema);
}

function validateLogin(user){
    const schema = {
        name: Joi.string().required().trim(),
        password: Joi.string().required().trim(),
    }
    return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;
exports.validateLogin = validateLogin;

//games: [gameSchema],
/*
        games: Joi.array().items(
            Joi.object({
                name: Joi.string().required().trim(),
                number: Joi.number().required().min(1).max(100),
                status: Joi.string().trim().required(),
                grade: Joi.number().min(1).max(100),
                startDate: Joi.date(),
                finishDate: Joi.date()
            })),
*/
const mongoose = require('mongoose');
const Joi = require('joi');
const status = require('./gameStatus');
const plat = require('./platforms');

const reviewSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: true,
        trim: true,
        get: value => value.charAt(0).toUpperCase()+value.slice(1),
        set: value => value.toLowerCase()
    },
    gameName: {
        type: String,
        required: true
    },
    gameNumber: {
        type: Number,
        required: true,
        min: 1
    },
    status: {
        type: String,
        enum: status,
        trim: true
    },
    grade: {
        type: Number,
        min: 1,
        max: 100,
        required: true,
        set: value => value * 10
    },
    platform: {
        type: String,
        enum: plat,
        min: 1,
        max: 1,
        required: true
    },
    fecha: {
        type: Date,
        required: true,
        default:  Date.now(),
        get: value => value.toDateString()
    },
    description: {
    	type: String,
        required: true,
        maxlength: 500,
        minlength: 5
    }
    /*startDate: {
        type: Date,
        get: value => value.toDateString()
    },
    finishDate: {
        type: Date,
        get: value => value.toDateString()
    }*/
}, { toJSON: { getters: true }});

const Review = mongoose.model('Review', reviewSchema);

function validateReview(review){
	 const schema = {
        userName: Joi.string().required().trim(),
        gameName: Joi.string().required(),
        gameNumber: Joi.number().required(),
        status: Joi.string().required(),
        startDate: Joi.date(),
        finishDate: Joi.date(),          
        grade: Joi.number().required().min(1).max(100),
        platform: Joi.string().required(),
        description: Joi.string().required().trim()
    }
    return Joi.validate(review, schema);
}

exports.Review = Review;
exports.validateReview = validateReview;

const mongoose = require('mongoose');
const generos = require('./genres');
const plat = require('./platforms');
const pubTipo = require('./publishers');
const Joi = require('joi');

const gameSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    number: {
        type: Number,
        required: true,
        unique: true,
        min: 1
    },
    releaseDate: {
        type: Date,
        required: true,
        get: value => value.toDateString()
    },
    metacritic: {
        type: Number,
        required: true,
        min: 1,
        max: 100
    },
    userGrade: {
        type: Number,
        default: 1,
        min: 1,
        max: 100
    },
    publisher: {
        type: String,
        required: true,
        enum: pubTipo
    },
    genres: {
        type: [{
            type: String,
            enum: generos
        }],
        min: 1,
        max: 3,
        required: true,
        get : v => v.map(value => value.charAt(0).toUpperCase()+value.slice(1))
    },
    platforms: {
        type: [{
            type: String,
            enum: plat
        }],
        min: 1,
        required: true,
        get : v => v.map(value => value.charAt(0).toUpperCase()+value.slice(1))
    },
    description: {
    	type: String,
        required: true
    },
    sprite: {
        type: String,
        required: true
    }
}, { toJSON: { getters: true }}); // Si quieren que sus getters funcionen en JSON hay que agregar el toJSON, si quieren que funcionen en objetos hay que agregar toObject

const Game = mongoose.model('Game', gameSchema);

function validateGame(game){
    const schema = {
        name: Joi.string().required().min(1).trim(),
        number: Joi.number().required().min(1),
        releaseDate: Joi.date().required(),
        metacritic: Joi.number().min(1).max(100).required(),
        userGrade: Joi.number().min(1).max(100),
        publisher: Joi.string().required().min(1).trim(),
        genres: Joi.array().required().min(1),
        platforms: Joi.array().required().min(1),
        description: Joi.string().min(1).required().trim(),
        sprite: Joi.string().min(1).required().trim()
    }
    return Joi.validate(game, schema);
}

exports.Game = Game;
exports.validateGame = validateGame;
const axios = require('axios');

async function agregarJuegos(){
    let game1 = {
        name: "Doki Doki Literature Club",
        number: 1,
        releaseDate: "2017-09-20",
        metacritic: 79,
        publisher: "Independiente",
        genres: ["Visual Novel"],
        platforms: ["PC"],
        description: "Nothing is Real",
        sprite: "https://i.ytimg.com/vi/BFSWlDpA6C4/maxresdefault.jpg"
    };
    let game2 = {
        name: "Assassin's Creed: Odyssey",
        number: 2,
        releaseDate: "2017-10-20",
        metacritic: 83,
        publisher: "Ubisoft",
        genres: ["Action-Adventure", "Open World", "Stealth"],
        platforms: ["PC", "Stadia", "PS4", "Xbox One"],
        description: "Escribe tu propia odisea épica y conviértete en un legendario héroe Spartan en Assassin’s Creed® Odyssey, \
                    una aventura inspiradora en la que tienes que forjar tu destino y definir tu propio camino en un mundo a punto de la destrucción. ",
        sprite: "https://cdn.shopify.com/s/files/1/0747/3829/products/mL2377_1024x1024.jpg?v=1539184609"
    };
    let game3 = {
        name: "God Of War",
        number: 3,
        releaseDate: "2018-04-25",
        metacritic: 94,
        publisher: "Sony",
        genres: ["Action", "Single-Player"],
        platforms: ["PS4"],
        description: "Kratos vuelve a empezar. Kratos, que vive como un hombre lejos de la sombra de los dioses, se adentrará en la\
                     salvaje tierra nórdica junto a su hijo Atreus, y ambos lucharán para completar una misión profundamente personal.",
        sprite: "https://los40es00.epimg.net/los40/imagenes/2018/03/20/videojuegos/1521544253_597018_1521544350_noticia_normal.jpg"
    };
    let game4 = {
        name: "The Witcher 3: Wild Hunt",
        number: 4,
        releaseDate: "2015-07-21",
        metacritic: 93,
        publisher: "CD Projekt Red",
        genres: ["Action-Adventure", "Open World", "Single-Player"],
        platforms: ["PC", "PS4", "Xbox One"],
        description: "Mientras la guerra se extiende por los Reinos del Norte, aceptarás el contrato de tu vida: encontrar a la niña\
                     de la profecía, un arma viviente que puede alterar el mundo tal y como lo conocemos.",
        sprite: "https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2017/11/The-Witcher-3-Wild-Hunt-1_0.jpg?itok=1Ft_YqcC"
    };
    let game5 = {
        name: "Spider-Man",
        number: 5,
        releaseDate: "2018-09-20",
        metacritic: 89,
        publisher: "Sony",
        genres: ["Action-Adventure", "Open World", "Single-Player"],
        platforms: ["PS4"],
        description: "Sony Interactive Entertainment, Insomniac Games y Marvel se han unido para crear una aventura de Spider-Man\
                     totalmente nueva y auténtica. No se trata del Spider-Man que has conocido antes ni visto en alguna otra parte. ",
        sprite: "https://i.ytimg.com/vi/3Pg1gs85SbA/maxresdefault.jpg"
    };
    let game6 = {
        name: "Uncharted 4: A Thief's End",
        number: 6,
        releaseDate: "2016-04-02",
        metacritic: 79,
        publisher: "Sony",
        genres: ["Action", "Single-Player"],
        platforms: ["PS4"],
        description: "Varios años después de su última aventura, el retirado cazador de fortunas, Nathan Drake, es obligado a\
                     regresar al mundo de los ladrones.",
        sprite: "https://i.ytimg.com/vi/hh5HV4iic1Y/maxresdefault.jpg"
    };
    
    try{
        await axios.post('http://127.0.0.1:3000/api/user/login', {name: "Daniel64", password: "hola12"})
        await axios.post('http://127.0.0.1:3000/api/game/register/', game1);
        await axios.post('http://127.0.0.1:3000/api/game/register', game2);
        await axios.post('http://127.0.0.1:3000/api/game/register', game3);
        await axios.post('http://127.0.0.1:3000/api/game/register', game4);
        await axios.post('http://127.0.0.1:3000/api/game/register', game5);
        await axios.post('http://127.0.0.1:3000/api/game/register', game6);
    }
    catch{
        console.log(`No se pudo.`);
    }
}

agregarJuegos();
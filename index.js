require('dotenv').config();
const winston = require('winston');
const passport = require('passport');
const session = require('express-session');
const path = require('path');
const mongoose = require('mongoose');
const express = require('express');
const app = express();
require('./passport/local');

app.use(session({
    secret: process.env.SECRET,
    saveUnitialized: false,
    resave: false
}));

app.use(express.json());
app.use(express.urlencoded( {extended: false }));

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.set('view engine', 'ejs');

require('./startup/logging')();
require('./startup/config')();
require('./startup/routes')(app);
require('./startup/db')();

const port = process.env.PORT || 3000;
module.exports = app.listen(port, () => console.log(`Escuchando en el puerto ${ port }`));



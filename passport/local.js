const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt');
const { User, validate,  validateLogin } = require('../models/user');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const user = await User.findById(id);
    done(null, user);
});

passport.use('local-register', new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password',
    passReqToCallback: true }, 
    async (req, username, password, done) => {
        if(req.body.confirm !== req.body.password)
        {
            return done(null, false, { message: 'Las conrtraseñas no coinciden'});;
        }

        if(Array.isArray(req.body.platforms))
        {
        }
        else
        {
            req.body.platforms = [req.body.platforms];
        }

        let temp = {name: username,
            birth: req.body.birth,
            email: req.body.email,
            password: password,
            description: req.body.description,
            platforms: Object.values(req.body.platforms)}

        const { error } = validate(temp);
        if (error) return done(error);

        let usuario = await User.findOne({ name: username}); 
        if (usuario) return done(null, false, { message: 'El usuario con ese nombre ya existe.'});
        
        usuario = new User({
            name: username,
            birth: req.body.birth,
            email: req.body.email,
            password: password,
            description: req.body.description,
            platforms: Object.values(req.body.platforms)
        });

        const salt = await bcrypt.genSalt(10);
        usuario.password = await bcrypt.hash(usuario.password, salt);
        await usuario.save();
        
        return done(null, usuario);
    }
));


passport.use('local-Admin', new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password',
    passReqToCallback: true }, 
    async (req, username, password, done) => {
        
        if(req.body.confirm !== req.body.password)
        {
            return done(null, false, { message: 'Las conrtraseñas no coinciden'});
        }
        console.log(req.body);
        if(req.body.adminpassword !== 'Konosuba')
        {
            return done(null, false, { message: 'esa no es la clave de administradores'});
        }
        
        if(Array.isArray(req.body.platforms))
        {
        }
        else
        {
            req.body.platforms = [req.body.platforms];
        }

        let temp = {name: username,
            birth: req.body.birth,
            email: req.body.email,
            password: password,
            description: req.body.description,
            platforms: Object.values(req.body.platforms),
            admin: true
        };

        const { error } = validate(temp);
        if (error) return done(error);

        let usuario = await User.findOne({ name: username}); 
        if (usuario) return done(null, false, { message: 'El usuario con ese nombre ya existe.'});
        
        usuario = new User({
            name: username,
            birth: req.body.birth,
            email: req.body.email,
            password: password,
            description: req.body.description,
            platforms: Object.values(req.body.platforms),
            admin: true
        });

        const salt = await bcrypt.genSalt(10);
        usuario.password = await bcrypt.hash(usuario.password, salt);
        await usuario.save();
        
        return done(null, usuario);
    }
));

passport.use('local-login', new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password',
    passReqToCallback: true }, 
    async (req, username, password, done) => {
        const { error } = validateLogin(req.body);
        if (error) return done(error);
        
        let usuario = await User.findOne({ name: username });
        
        if (!usuario) {
            return done(null, false, { message: 'El usuario no existe'});
        }

        const validPassword = await bcrypt.compare(password, usuario.password);
        if (!validPassword) {
            return done(null, false, { message: 'El password es incorrecto.'});
        }
        const token = usuario.generarAuthToken();
        
        return done(null, usuario);
    }
));



passport.use('local-apiLogin', new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password',
    passReqToCallback: true }, 
    async (req, username, password, done) => {
        const { error } = validateLogin(req.body);
        if (error) return done(error);
        
        let usuario = await User.findOne({ name: username });
        
        if (!usuario) {
            console.log('El usuario no existe');
            return done(null, false, { message: 'El usuario no existe'});
        }
        
        const token = usuario.generarAuthToken();
        //req.user['x-auth-token'] = token;
        return done(null, usuario);
    }
));


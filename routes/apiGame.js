
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { Game, validate } = require('../models/game');
const { Review, validateReview} = require('../models/review');


router.get('/', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    
    let game = await Game
        .find()
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, publisher: 1, releaseDate: 1, 
                    metacritic: 1, userGrade: 1})
        .sort()
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    
    res.send(game);
});



router.post('/register', auth, async (req, res) =>{
    if(!req.user.admin)
    {
        return res.status(403).send( 'Debes ser un administrador para acceder a estas opciones');
    }

    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details);

    let game = await Game.findOne({ number: req.body.number }); 
    if (game) return res.status(400).send('El Juego con ese número ya existe.');

    game = new Game({
        name: req.body.name,
        number: req.body.number,
        releaseDate: req.body.releaseDate,
        metacritic: req.body.metacritic,
        publisher: req.body.publisher,
        description: req.body.description,
        genres: req.body.genres,
        platforms: req.body.platforms,
        sprite: req.body.sprite
    });
    await game.save();
    return res.status(201).redirect('/game/'+req.body.number);
});

router.get('/publishers/:nombrePublisher', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    let game = await Game
        .find({publisher: req.params.nombrePublisher})
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, releaseDate: 1})
        .sort({ number: 1 })
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    
    res.status(200).send(game);
});

router.get('/genre/:nombreGenre', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    let game = await Game
        .find({genres: req.params.nombreGenre})
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, releaseDate: 1})
        .sort({ number: 1 })
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    res.status(200).send(game);
});

router.get('/:id', async (req, res) =>{

    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).send('No se encontró el Juego');
    
    let reviews = await Review
        .find({ gameName: game.name })
        .select()
        .limit(10)
        .skip(0);
    return res.status(200).send(reviews);
});

router.post('/addReview/:id', auth, async (req, res) => { 

    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).send('No se encontró el Juego');
    let review = await Review.findOne({ userName: req.user.name, gameName: game.name}); 
    if (review) return res.status(404).send('Y reseñaste antes perro');

    review = new Review({
        userName: req.user.name,
        gameName: game.name,
        status: req.body.status,
        grade: req.body.grade,
        platform: req.body.platform,
        description: req.body.description,
        gameNumber: game.number
    });

    await review.save();

    let grade = await Review.aggregate([
        { $match: {"gameName": game.name}},
        { 
            $group: {
                _id: null, 
                total: {
                    $sum: "$grade"
                },
                average_amount: {
                    $avg: "$grade"
                }
            }
        }]);
    const updatedGame= await Game.findOneAndUpdate({ number: req.params.id }, {
        $set :{"userGrade": grade[0].average_amount}
        }, {new: true});
    await updatedGame.save();
    return res.status(201).send(review);
});

router.put('/updateGame/:id', /*auth,*/ async (req, res) =>{
    /*if(!req.user.admin)
    {
        return res.status(403).send( 'Debes ser un administrador para acceder a estas opciones');
    }*/

    const game = await Game
        .findOne({ number: req.params.id});
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego'});

    let gameName = game.name;
    const updatedGame = await Game.findByIdAndUpdate(game._id, {
        $set : req.body
    }, {new: true});
    await updatedGame.save();

    try{
        const freshReviews = await Review.updateMany({"gameName": gameName}, { $set: { gameName: updatedGame.name,
                        gameNumber: updatedGame.number} });

        res.send(updatedGame);
    }
    catch(e){
        console.log(e);
    }

});

module.exports = router;
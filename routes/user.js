const passport = require('passport');
const express = require('express');
const auth = require('../middleware/auth');
const router = express.Router();
const { User, validate } = require('../models/user');
const { Game } = require('../models/game');
const { Review, validateReview} = require('../models/review');


router.get('/login', (req, res) => {
	if(req.user) return res.redirect('/user/me');
    let failLogin = req.query.rechazado === 'true';
    res.render('login', {conectado: false, admin: false, failLogin});
});

router.post('/login', passport
    .authenticate('local-login', {
        successRedirect: '/user/me',
        failureRedirect: '/user/login?rechazado=true',
        passReqToCallback: true
}));

router.get('/signup', (req, res) => {
	if(req.user) req.logOut();
    res.render('new_user', {conectado: false, admin: false});
});

router.post('/signup', passport.authenticate('local-register', {
     successRedirect: '/user/me',
     failureRedirect: '/user/signup',
     passReqToCallback: true
}));

router.get('/logout', (req, res) => {
	req.logOut();
	res.redirect('/user/login');
});

router.get('/me', auth, async (req, res) =>{
    const usuario = await User
        .findOne({ name: req.user.name });
    let esAdmin = req.user.admin;
    
    if (!usuario) return res.status(404).render('error', { error: 404, message: 'No se encontró el Usuario',
					conectado: true, admin: esAdmin});
    return res.render('user', { user: usuario, conectado: true, admin: esAdmin });
});

router.get('/misJuegos', auth, async (req, res) => {
	let findConditionString = req.query['condition'] || "Todos";
	let findCondition;
	if(findConditionString === "Todos")
	{
		findCondition= {userName: req.user.name};
	}	
	else if(findConditionString === "Pendiente")
	{
		findCondition = {userName: req.user.name,status: "Pendiente"};
	}
	else if(findConditionString === "Jugando")
	{
		findCondition = {userName: req.user.name,status: "Jugando"};
	}
	else if(findConditionString === "Terminado")
	{
		findCondition = {userName: req.user.name,status: "Terminado"};
	}

    let esAdmin = req.user.admin;
	let reviews = await Review
        .find(findCondition);
    return res.render('misJuegos', {conectado: true, reviews, findConditionString, admin: esAdmin});
});


router.get('/misAmigos', auth, async (req, res) => {

    let esAdmin = req.user.admin;

    let users = await User
        .find({seguidores: req.user.name})
        .sort({ name: 1 });

    return res.render('misAmigos', {conectado: true, users, admin: esAdmin});
});

router.post('/borrarReview/:id', auth, async (req, res) => {
	let esAdmin = req.user.admin;
	const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego',
				conectado: true, admin: esAdmin});

    try
	{
		let review = await Review.findOneAndDelete({gameName: game.name, userName: req.user.name});
		let grade = await Review.aggregate([
	        { $match: {"gameName": game.name}},
	        { 
	            $group: {
	                _id: null, 
	                total: {
	                    $sum: "$grade"
	                },
	                average_amount: {
	                    $avg: "$grade"
	                }
	            }
	        }]);
		
		let score = 1;
		if (typeof grade !== 'undefined' && grade.length > 0) 
		{
			score = grade[0].average_amount;
		}
		else
		{
			console.log("ESta vacío");
			
		}
	    const updatedGame= await Game.findOneAndUpdate({ number: req.params.id }, {
	        $set :{"userGrade": score}
	        }, {new: true});
	    await updatedGame.save();
	}catch (e){
		return res.status(404).render('error', { error: 404, message: 'no se pudo', conectado: true, admin: esAdmin});
	}
	return res.status(201).redirect('/user/misJuegos');
});

router.post('/cambiarStatus/:id', auth, async (req, res) => {
	let esAdmin = req.user.admin;

	let newStatus = req.body["status" + req.params.id];
	try{
		const updatedReview= await Review.findOneAndUpdate({ gameNumber: req.params.id, userName: req.user.name }, {
	        $set :{"status" : newStatus}
	        }, {new: true});
		await updatedReview.save();
		return res.status(201).redirect('/user/misJuegos');
	}catch (e){
		return res.status(404).render('error', { error: 404, message: 'no se pudo acutalizar la Review',
			 conectado: true, admin: esAdmin});
	}
});

router.get('/:other', async (req, res) =>{	
    const usuario = await User
        .findOne({ name: req.params.other });
    
    let conectado = false;
    let esAdmin = false;
    let siguiendo = false;
    let self  = false;

    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
		siguiendo = usuario.seguidores.includes(req.user.name);
    	self = usuario.name === req.user.name;
    }
    if (!usuario) return res.status(404).render('error', { error: 404, message: 'No se encontró el Usuario', conectado, admin: esAdmin});
    

    return res.render('other', { user: usuario, conectado, admin: esAdmin, siguiendo, self});
});

router.post('/seguir/:id', auth, async (req, res) => {
	const usuario = await User
        .findOne({ name: req.params.id });
    if (!usuario) return res.status(404).render('error', { error: 404, message: 'No se encontró el Usuario'});
    let amigo = await User
        .findOne({seguidores: req.user.name, name: usuario.name})
        .sort({ name: 1 });
    if(amigo) return res.status(404).render('error', { error: 404, message: 'Ya lo estás siguiendo.'});

    let updatedUser = await User.findByIdAndUpdate(usuario._id,
	    {$push: {seguidores: req.user.name}},
	    {safe: true, upsert: true},
	    function(err, doc) {
	        if(err){
	        console.log(err);
	        }else{
	        //do stuff
	        }
	    });
    
    await updatedUser.save();
    return res.status(201).redirect('/user/' + req.params.id);
});

router.post('/noSeguir/:id', auth, async (req, res) => {
	const usuario = await User
        .findOne({ name: req.params.id });
    if (!usuario) return res.status(404).render('error', { error: 404, message: 'No se encontró el Usuario'});

    let updatedUser = await User.findByIdAndUpdate(usuario._id,
	    {$pull: {seguidores: req.user.name}},
	    {safe: true, upsert: true},
	    function(err, doc) {
	        if(err){
	        console.log(err);
	        }else{
	        //do stuff
	        }
	    });
    
    await updatedUser.save();
    return res.status(201).redirect('/user/' + req.params.id);
});

router.get('/:other/susJuegos', async (req, res) =>{
	let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    } 
    let reviews = await Review
        .find({userName: req.params.other});
    return res.render('susJuegos', {conectado, reviews, usuario: req.params.other, admin: esAdmin});
});


router.post('/modifiyProfile', auth, async (req, res) => {
    let previousName = req.user.name;
    let previousEmail = req.user.email;
    let previousAdmin = req.user.admin;
    let previousSeguidores = req.user.seguidores;

    const userObj =  {name: req.body.name,
                email: previousEmail,
                birth: req.body.birth,
                password: req.body.password,
                description: req.body.description,
                platforms: req.body.platforms,
                admin: previousAdmin,
                seguidores: previousSeguidores
            };
    const { error } = validate(userObj);
    if (error) return res.status(400).render('error', {error: 400, message: error, admin: true, conectado: true});

    const updatedUser = await User.findByIdAndUpdate({name: req.user.name}, {
        $set : userObj
    }, {new: true});
    await updatedUser.save();
    

    try{
        const freshReviews = await Review.updateMany({"userName": previousName}, { $set: { userName: updatedUser.name,
                        gameNumber: updatedGame.number} });

        await User.updateMany({seguidores: previousName} ,
        {$push: {seguidores: updatedUser.name}},
        {safe: true, upsert: true},
        function(err, doc) {
            if(err){
            console.log(err);
            }else{
            //do stuff
            }
        });

        await User.updateMany({seguidores: previousName} ,
        {$pull: {seguidores: previousName}},
        {safe: true, upsert: true},
        function(err, doc) {
            if(err){
            console.log(err);
            }else{
            //do stuff
            }
        });

        res.status(200).redirect("user/me");
    }
    catch(e){
        console.log(e);
    }
});


module.exports = router;





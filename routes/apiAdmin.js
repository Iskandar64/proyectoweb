const passport = require('passport');
const express = require('express');
const auth = require('../middleware/auth');
const router = express.Router();
const { User, validate } = require('../models/user');
const { Game } = require('../models/game');
const { Review } = require('../models/review');


router.post('/signAdmin', passport.authenticate('local-Admin', {
     successRedirect: '/user/me',
     failureRedirect: '/admin/signAdmin',
     passReqToCallback: true
}));


router.delete('/deleteGame/:id', auth, async (req, res) => {
	if(!req.user.admin) return res.status(403).send('Debes ser un administrador para acceder a estas opciones');
	const game = await Game.findOne({ number: req.params.id});
	if (!game) return res.status(404).send('El juego que quiere borrar no existe');
	const eraseGame = await Game.findOneAndDelete({number: req.params.id});
	await game.save();
	try
	{
		await Review.deleteMany({"gameName": eraseGame.name});
		return res.status(201).send(game);
	}catch (e){
		return res.status(404).send(e);
	}
});

router.delete('/deleteUser/:id', auth, async (req, res) => {
	if(!req.user.admin) return res.status(403).send('Debes ser un administrador para acceder a estas opciones');
	const user = await User.findOne({ name: req.params.id});
	if (!user) return res.status(404).send('El usuario que quiere borrar no existe');
	const eraseUser = await User.deleteOne({name: req.params.id});
	await user.save();
	try
	{
		await Review.deleteMany({"userName": req.params.id});
		return res.status(201).send(eraseUser);
	}catch (e){
		return res.status(404).send(e);
	}
	
});

module.exports = router;
const passport = require('passport');
const express = require('express');
const auth = require('../middleware/auth');
const router = express.Router();
const { User, validate } = require('../models/user');
const { Game, validateGame } = require('../models/game');
const { Review } = require('../models/review');

router.get('/', auth, async(req, res) => {
	if(!req.user.admin) return res.status(403).render('error', { error: 403, 
		message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});
	res.render('adminOpciones', {conectado: true, admin: true});
});

router.get('/signAdmin', (req, res) => {
    res.render('signAdmin', {conectado: false, admin: false});
});

/*router.post('/signAdmin', (req, res) => {
     console.log(req.body);
     console.log("Estoy aqui");
     return res.status(201).redirect('/admin/signAdmin');
 });*/
router.post('/signAdmin', passport.authenticate('local-Admin', {
     successRedirect: '/user/me',
     failureRedirect: '/admin/signAdmin',
     passReqToCallback: true
}));

router.get('/deleteGame', auth, async(req, res) => {
	if(!req.user.admin) return res.status(403).render('error', { error: 403, 
		message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});
	let games = await Game.find().sort({releaseDate : -1});
	res.render('deleteGame', { games, conectado: true, admin: true});
});

router.post('/deleteGame/:id', auth, async (req, res) => {
	if(!req.user.admin) return res.status(403).render('error', { error: 403, 
		message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});

	const game = await Game.findOne({ number: req.params.id});

	if (!game) return res.status(404).render('error', { error: 404, message: 'El juego que quiere borrar no existe',
		 admin: true, conectado: true});
	const eraseGame = await Game.findOneAndDelete({number: req.params.id});
	await eraseGame.save();
	try
	{
		await Review.deleteMany({"gameName": eraseGame.name});
	}catch (e){
		return res.status(404).render('error', { error: 404, message: e, conectado: true, admin: true});
	}
	return res.status(201).redirect('/admin/deleteGame');
});

router.get('/deleteUser', auth, async(req, res) => {
	if(!req.user.admin) return res.status(403).render('error', { error: 403, 
		message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});
	let users = await User.find({admin: {$eq: false}}).sort({name: -1});
	res.render('deleteUser', { users, conectado: true, admin: true});
});

router.post('/deleteUser/:id', auth, async (req, res) => {
	if(!req.user.admin) return res.status(403).render('error', { error: 403, 
		message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});
	const user = await User.findOne({ name: req.params.id});
	if (!user) return res.status(404).render('error', { error: 404, message: 'El usuario que quiere borrar no existe',
		conectado: true, admin: true});
	const eraseUser = await User.deleteOne({name: req.params.id});
	await user.save();
	try
	{
		await Review.deleteMany({"userName": req.params.id});
	}catch (e){
		return res.status(404).render('error', { error: 404, message: e, conectado: true, admin: true});
	}
	return res.status(201).redirect('/admin/deleteUser');
});

module.exports = router;
const passport = require('passport');
const express = require('express');
const auth = require('../middleware/authApi');
const router = express.Router();
const { User, validate } = require('../models/user');


router.post('/login', (req, res) => {
    passport.authenticate('local-signup', (err, user, info) => {
    if (err) return res.status(400).send('No se encontró al usuario');

    if (info) return res.status(409).send(info);
    if (user) {
        req.logIn(user, (err) => {
            if (err) return res.status(500).render('error', { error: 500, message: 'Error al crear sesión...' });
            return res.status(201).send(user.generarAuthToken());
        });
    }
    })(req, res);
});


router.post('/signup', passport.authenticate('local-register', {
     successRedirect: '/api/user/me',
     failureRedirect: '/user/signup',
     passReqToCallback: true
}));


router.get('/me', auth, async (req, res) =>{
    const usuario = await User
        .findOne({ name: req.user.name });
    if (!usuario) return res.status(404).send('No se encontró el Usuario');
    return res.status(200).send(usuario);
});

router.get('/:other', async (req, res) =>{  
    const usuario = await User
        .findOne({ name: req.params.other });
    if (!usuario) return res.status(404).send('No se encontró el Usuario');
      
    return res.status(200).send(usuario);
});

router.get('/:other/susJuegos', async (req, res) =>{
    let reviews = await Review
        .find({userName: req.params.other});
    if (!reviews) return res.status(404).send(`No se encontraron reseñas de ${req.params.other}`);
    return res.status(200).send(reviews);
});

router.get('/misJuegos', auth, async (req, res) => {
    let reviews = await Review
        .find(findCondition);
    return res.send(200).send(reviews);
});

router.post('/borrarReview/:id', auth, async (req, res) => {
    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).send('No se encontró el Juego');

    try
    {
        let review = await Review.findOneAndDelete({gameName: game.name, userName: req.user.name});
        let grade = await Review.aggregate([
            { $match: {"gameName": game.name}},
            { 
                $group: {
                    _id: null, 
                    total: {
                        $sum: "$grade"
                    },
                    average_amount: {
                        $avg: "$grade"
                    }
                }
            }]);
        
        let score = 1;
        if (typeof grade !== 'undefined' && grade.length > 0) 
        {
            score = grade[0].average_amount;
        }
        const updatedGame= await Game.findOneAndUpdate({ number: req.params.id }, {
            $set :{"userGrade": score}
            }, {new: true});
        await updatedGame.save();
        return res.status(201).send(review);
    }catch (e){
        return res.status(404).send(`Error al intentar actualizar información debido a ${e}`);
    } 
});

router.post('/cambiarStatus/:id', auth, async (req, res) => {
    try{
        const updatedReview= await Review.findOneAndUpdate({ gameNumber: req.params.id, userName: req.user.name }, {
            $set :{"status" : req.body.status}
            }, {new: true});
        await updatedReview.save();
        return res.status(201).send(updatedReview);
    }catch (e){
        return res.status(404).send(`no se pudo acutalizar la Review debido a ${e}`);
    }
});

router.put('/modifiyProfile', auth, async (req, res) => {
    let previousName = req.user.name;
    let previousEmail = req.user.email;
    let previousAdmin = req.user.admin;
    let previousSeguidores = req.user.seguidores;

    const updatedUser = await User.findByIdAndUpdate({name: req.user.name}, {
        $set :  {name: req.body.name,
                email: previousEmail,
                birth: req.body.birth,
                password: req.body.password,
                description: req.body.description,
                platforms: req.body.platforms,
                admin: previousAdmin,
                seguidores: previousSeguidores
            }
    }, {new: true});
    await updatedUser.save();
    

    try{
        const freshReviews = await Review.updateMany({"userName": previousName}, { $set: { userName: updatedUser.name,
                        gameNumber: updatedGame.number} });

        await User.updateMany({seguidores: previousName} ,
        {$push: {seguidores: updatedUser.name}},
        {safe: true, upsert: true},
        function(err, doc) {
            if(err){
            console.log(err);
            }else{
            //do stuff
            }
        });

        await User.updateMany({seguidores: previousName} ,
        {$pull: {seguidores: previousName}},
        {safe: true, upsert: true},
        function(err, doc) {
            if(err){
            console.log(err);
            }else{
            //do stuff
            }
        });

        res.status(200).send(updatedUser);
    }
    catch(e){
        console.log(e);
    }
});



module.exports = router;





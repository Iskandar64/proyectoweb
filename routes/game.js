
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { Game, validateGame } = require('../models/game');
const { Review, validateReview} = require('../models/review');
const size = require('window-size');
var sr = require('screenres');

function chunk(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
      const last = chunked_arr[chunked_arr.length - 1];
      if (!last || last.length === size) {
        chunked_arr.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }
    return chunked_arr;
}

router.get('/', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    
    const dim = sr.get();
    if(dim[0] < dim[1]) 
    {
        columns = 1;
        console.log(dim[0]);
    }

    const sortConditionNumber = parseInt(req.query['condition']) || 1;
    let sortConditionString;

    if(sortConditionNumber === 1)
    {
    	sortConditionString = {userGrade : -1};
    }
    else if(sortConditionNumber === 2)
    {
    	sortConditionString = {releaseDate : -1};
    }
    else if(sortConditionNumber === 3)
    {
    	sortConditionString = {metacritic : -1};
    }
    else
    {
    	sortConditionString = {name : 1};
    }

    let game = await Game
        .find()
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, publisher: 1, releaseDate: 1, 
                    metacritic: 1, userGrade: 1})
        .sort(sortConditionString)
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    game = chunk(game, columns);

    let atras = true;
    if(pageNumber < 2 )
    {
    	atras = false;
    }
    let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    } 
    res.status(200).render('gamelist', { game, pageNumber, pageSize, columns, atras, sortConditionNumber, conectado, admin: esAdmin });
});



router.get('/register', auth, async (req, res) => {
    if(!req.user.admin)
    {
        return res.status(403).render('error', { error: 403, message: 'Debes ser un administrador para acceder a estas opciones',
            conectado: true, admin: false});
    }
	res.render('new_game', {conectado: true, admin: true});
});

router.post('/register', auth, async (req, res) =>{
    if(!req.user.admin)
    {
        return res.status(403).render('error', { error: 403, message: 'Debes ser un administrador para acceder a estas opciones', 
                    conectado: true, admin: false});
    }


    if(req.body.genres === undefined || req.body.platforms === undefined)
    {
        return res.status(400).render('error', {error: 400, message: 'No ingresaste datos a la forma', 
                admin: true, conectado: true});
    }

    if(typeof req.body.genres !==  'object')
    {
        req.body.genres = {1: req.body.genres};
    }

    if(typeof req.body.platforms !==  'object')
    {
        req.body.platforms = {1: req.body.platforms};
    }

    
    let arrayGenre = Object.values(req.body.genres);
    if(arrayGenre.length >3)
    {
        arrayGenre = arrayGenre.slice(0, 3);
    }
    req.body.genres = arrayGenre;
    let arrayPlat = Object.values(req.body.platforms);
    req.body.platforms = arrayPlat;
    

	const { error } = validateGame(req.body);
    if (error) return res.status(400).render('error', {error: 400, message: error, admin: true, conectado: true});
    let game = await Game.findOne({ number: req.body.number }); 
    if (game) return res.status(400).render('error', {error: 400, message: 'El Juego con ese número ya existe.', 
                    admin: true, conectado: true});

	game = new Game({
		name: req.body.name,
		number: req.body.number,
		releaseDate: req.body.releaseDate,
		metacritic: req.body.metacritic,
		publisher: req.body.publisher,
		description: req.body.description,
		genres: req.body.genres,
		platforms: req.body.platforms,
		sprite: req.body.sprite
	});
	await game.save();
	return res.status(201).redirect('/game/'+req.body.number);
});

router.get('/publishers', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    const nombrePublisher = req.query.publisher;
    let game = await Game
        .find({publisher: nombrePublisher})
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, releaseDate: 1})
        .sort({ number: 1 })
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    game = chunk(game, columns);
    let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    } 
    
    let atras = true;
    if(pageNumber < 2 )
    {
        atras = false;
    }
    res.status(200).render('publisherGames', { game, atras, pageNumber, pageSize, columns, nombrePublisher,
                     conectado, admin: esAdmin });
});

router.get('/genre/:nombreGenre', async (req, res) => {
    const pageSize = parseInt(req.query['pageSize']) || 20;
    let pageNumber = parseInt(req.query['pageNumber']) || 1;
    const columns = parseInt(req.query['columns']) || 3;
    let game = await Game
        .find({genres: req.params.nombreGenre})
        .select({ _id: 0, name:1, number: 1, sprite: 1, genres: 1, releaseDate: 1})
        .sort({ number: 1 })
        .limit(pageSize)
        .skip((pageNumber-1)*pageSize);
    game = chunk(game, columns);
    let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    }

    let atras = true;
    if(pageNumber < 2 )
    {
        atras = false;
    }
    res.status(200).render('genreGames', { game, pageNumber, atras, pageSize, columns, nombreGenre: req.params.nombreGenre,
                     conectado, admin: esAdmin });
});

router.get('/:id', async (req, res) =>{
    let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    } 

    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego', conectado, admin: esAdmin});
    
    let reviewAble = true;
    if(req.user)
    {
        let review = await Review.findOne({ userName: req.user.name, gameName: game.name}); 
        if(review){ reviewAble = false; }
    }
    else
    {
        reviewAble = false;
    }

    let reviews = await Review
        .find({ gameName: game.name })
        .select()
        .limit(10)
        .skip(0);
    return res.status(200).render('game', { game,  conectado, platforms: game.platforms, reviewAble, reviews, admin: esAdmin});
});

router.post('/addReview/:id', auth, async (req, res) => {
    let conectado = false;
    let esAdmin = false;
    if(req.user){
        conectado = true;
        esAdmin = req.user.admin;
    } 

    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego',
                        conectado, admin: esAdmin});
    let review = await Review.findOne({ userName: req.user.name, gameName: game.name}); 
    if (review) return res.status(404).render('error', { error: 404, message: 'Y reseñaste antes perro',
                        conectado, admin: esAdmin});

    review = new Review({
        userName: req.user.name,
        gameName: game.name,
        status: req.body.status,
        grade: req.body.grade,
        platform: req.body.platform,
        description: req.body.description,
        gameNumber: game.number
    });

    await review.save();

    let grade = await Review.aggregate([
        { $match: {"gameName": game.name}},
        { 
            $group: {
                _id: null, 
                total: {
                    $sum: "$grade"
                },
                average_amount: {
                    $avg: "$grade"
                }
            }
        }]);
    const updatedGame= await Game.findOneAndUpdate({ number: req.params.id }, {
        $set :{"userGrade": grade[0].average_amount}
        }, {new: true});
    await updatedGame.save();
    return res.status(200).status(201).redirect('/game/'+req.params.id);
});

router.get('/updateGame/:id', auth, async(req, res) => {
    if(!req.user.admin)
    {
        return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego',
                        conectado: true, admin: false});
    }

    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego',
                        conectado: true, admin: true});
    return res.status(404).render('changeGame', { game, conectado: true, admin: true});
});

router.post('/updateGame/:id', auth, async (req, res) =>{
    if(!req.user.admin)
    {
        return res.status(403).render('error', { error: 404, 
            message: 'Debes ser un administrador para acceder a estas opciones', conectado: true, admin: false});
    }

    
    const game = await Game
        .findOne({ number: req.params.id });
    if (!game) return res.status(404).render('error', { error: 404, message: 'No se encontró el Juego',
                        conectado, admin: true});

    if(typeof req.body.genres !==  'object')
    {
        req.body.genres = {1: req.body.genres};
    }
    if(typeof req.body.platforms !==  'object')
    {
        req.body.platforms = {1: req.body.platforms};
    }

    
    let arrayGenre = Object.values(req.body.genres);
    if(arrayGenre.length >3)
    {
        arrayGenre = arrayGenre.slice(0, 3);
    }
    req.body.genres = arrayGenre;
    let arrayPlat = Object.values(req.body.platforms);
    req.body.platforms = arrayPlat;
    req.body.number = game.number;

    const { error } = validateGame(req.body);
    if (error) return res.status(400).render('error', {error: 400, message: error, admin: true, conectado: true});

    let gameName = game.name;
    const updatedGame = await Game.findByIdAndUpdate(game._id, {
        $set : req.body
    }, {new: true});
    await updatedGame.save();

    try{
        const freshReviews = await Review.updateMany({"gameName": gameName}, { $set: { gameName: updatedGame.name,
                        gameNumber: updatedGame.number} });

        return res.status(201).redirect('/game/'+updatedGame.number);
    }
    catch(e){
        return res.status(403).render('error', { error: 404, 
            message: e, conectado: true, admin: false});
    }

});

module.exports = router;
const { User } = require('../../models/user');
const request = require('supertest');
const mongoose = require('mongoose');
let server;

describe('/api/user', () => {

    beforeAll(() => { server = require('../../index'); });
    
    afterAll(async (done) => {
        await User.deleteMany({name: {$in: ['Rem', 'Emilia', 'Kratos']}}, function(err) {});
        server.close();         
        await mongoose.connection.close();
        done();
    });
    
    describe('POST /', () => {

        it('should return a token', async (done) => {

            const userNew = {
                name: 'Emilia',
                password
            }
            
            const res = await request(server).get('/api/user/signup');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(20);
            expect(res.body.some(user => pokemon.name === 'Venusaur')).toBeTruthy();
            expect(res.body.some(user => pokemon.name === 'Charizard')).toBeTruthy();
            expect(res.body.some(user => pokemon.number === 9)).toBeTruthy();
            done();
        });
    });

    
});
const mongoose = require('mongoose');
const winston = require('winston');

module.exports = function() {
    mongoose.connect(process.env.DB_CON, { useNewUrlParser: true, useCreateIndex: true })
        .then(() => winston.info('Conectado a MongoDB...'))
}
const express = require('express');
const flash = require('connect-flash');
const error = require('../middleware/error');
const gameRouter = require('../routes/game');
const rootRouter = require('../routes/root');
const userRouter = require('../routes/user');
const adminRouter = require('../routes/admin');
const apiUser = require('../routes/apiUser');
const apiGame = require('../routes/apiGame');
const apiAdmin = require('../routes/apiAdmin')

module.exports = function(app) {
    app.use(express.json());
    app.use(flash());
    app.use('/', rootRouter);
    app.use('/game', gameRouter);
	app.use('/user', userRouter);
	app.use('/admin', adminRouter);
	app.use('/api/game', apiGame)
	app.use('/api/user', apiUser);
	app.use('/api/admin', apiAdmin);
    app.use(error);
}
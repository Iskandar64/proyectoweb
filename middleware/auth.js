const jwt = require('jsonwebtoken');

function auth(req, res, next) {
    if (req.isAuthenticated()) return next();
    console.log("no pasas");
    return res.status(401).redirect('/user/login');
}

module.exports = auth;
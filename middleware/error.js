const winston = require('winston');

module.exports = function(err, req, res, next){
    winston.error(err.message, err);
    console.log(`errorInfo: ${err}`);
    res.status(500).render('error', { error: 500, message: err.message,
					conectado: false, admin: false});
}
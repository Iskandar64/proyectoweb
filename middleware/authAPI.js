const jwt = require('jsonwebtoken');

function auth (req, res, next){
    const token = req.user['x-auth-token'];
    console.log(token);
    if (!token) return res.status(401).send('No puedes pasar.');

    try{
        const decoded = jwt.verify(token, process.env.SECRET);
        req.user = decoded._id;
        next();
    }
    catch(ex) {
        res.status(400).send('Bad request.');
    }
}

module.exports = auth;